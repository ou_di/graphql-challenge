/**
 * Theme for styled components
 * @author Oleg Durybaba <mail@oleg.durybaba.com>
 */

export default {
  color0: '#000',
  color1: '#fff',
  color2: '#f44336', // red
  color4: '#f7f7f7', // light grey
  color5: '#aaaaaa', // grey
  color6: '#333', // dark grey
  color7: '#ccc', // medium grey
  color8: '#f2f2f2', // medium light grey
  color9: '#256fd5', // blue
  font: {
    family: 'arial, helvetica, sans-serif',
    size: '1rem'
  }
}
