/**
 * App component
 * The main App entry point, with Header, Navigation and styled compoents theme provider
 * @author Oleg Durybaba <mail@oleg.durybaba.com>
 */

import React, { Component } from 'react'
import styled, { ThemeProvider } from 'styled-components'
import Theme from './css/theme'
import Header from './components/Header'

const Content = styled.div`
  padding: 1rem;
  text-align: center;
`

class App extends Component {
  render() {
    return (
      <ThemeProvider theme={Theme}>
        <main>
          <Header />
          <Content>{this.props.children}</Content>
        </main>
      </ThemeProvider>
    )
  }
}

export default App
