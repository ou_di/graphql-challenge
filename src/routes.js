/**
 * App routes.
 * @author Oleg Durybaba <mail@oleg.durybaba.com>
 */
import React from 'react'
import { Router, Route, Redirect } from 'react-router-dom'
import createBrowserHistory from 'history/createBrowserHistory'
import createMemoryHistory from 'history/createMemoryHistory'
import App from './App'
import About from './components/About'
import Home from './components/Home'

const browserHistory = process.browser
  ? createBrowserHistory()
  : createMemoryHistory()

export const history = browserHistory

export const PATH_HOME = '/home'
export const PATH_ABOUT = '/about'

const Routes = props => {
  return (
    <Router history={browserHistory}>
      <App>
        <Route exact path="/" render={() => <Redirect to="/home" />} />
        <Route path={PATH_ABOUT} component={About} />
        <Route path={PATH_HOME} component={Home} />
      </App>
    </Router>
  )
}

export default Routes
