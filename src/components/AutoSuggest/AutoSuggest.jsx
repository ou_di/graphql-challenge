// @flow

/**
 * AutoSuggest component
 * This component is using paypal's downshift
 * to manage search inputs and to output the search results on the fly
 * @author Oleg Durybaba <mail@oleg.durybaba.com>
 */
import React, { PureComponent } from 'react'
import Downshift from 'downshift'
import styled from 'styled-components'

const SearchWrapper = styled.div`
  max-width: 400px;
  margin: 0 auto;
  position: relative;
`
const SearchBoxStyle = `
  border-radius: 2px;
  box-sizing: border-box;
  display: block;

`
const InputStyled = styled.input`
  ${SearchBoxStyle};
  border: 1px solid ${p => p.theme.color5};
  padding: 0.5rem;
  width: calc(100% - 50px);
`
const Results = styled.div`
  ${SearchBoxStyle};
  margin-top: 1px;
  width: 100%;
  max-height: 300px;
  overflow: auto;
  border: 1px solid ${p => p.theme.color5};
  text-align: left;
  > div {
    border-bottom: 1px solid ${p => p.theme.color4};
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    padding: 0.5rem;

    a {
      color: ${p => p.theme.color0};
    }

    &:hover {
      background: ${p => p.theme.color9};
      color: ${p => p.theme.color1};
      span {
        background: ${p => p.theme.color1};
        color: ${p => p.theme.color9};
      }

      a {
        color: ${p => p.theme.color1};
      }
    }
  }
`
const SearchInFilter = styled.div`
  position: absolute;
  right: -10px;
  top: 5px;
  padding-right: 0.5rem;
  button {
    width: 40px;
    height: 20px;
    border-radius: 10px;
    border: 1px solid ${p => p.theme.color5};
    position: relative;
  }
`

const Handle = styled.span`
  display: inline-block;
  width: 20px;
  height: 20px;
  border-radius: 20px;
  background: ${p => p.theme.color9};
  position: absolute;
  top: -1px;
  left: 0;
  transition: transform 0.2s;
  transform: translateX(${p => (p.xPos ? 0 : '100%')});
`
const StateLabel = styled.span`
  width: 2rem;
  text-align: center;
  font-size: 0.4rem;
  margin-right: 0.5rem;
  display: inline-block;
  padding: 0.2rem;
  color: ${p => p.theme.color1};
  border-radius: 5px;
  background: ${p => p.theme.color9};
  position: relative;
  top: -3px;
`
const NoResults = styled.div``

type Props = {
  items: [],
  onChange: () => void
}

type State = {
  searchIn: string,
  selectedValue: string
}

class AutoSuggest extends PureComponent<Props, State> {
  state = {
    searchIn: 'title',
    selectedValue: ''
  }

  onChange = (item: Object) => {
    this.setState({ selectedValue: item[this.state.searchIn] })
  }

  handleKeyDown = (e: Object) => {
    switch (e.keyCode) {
      case 8: //backspace
        this.setState({ selectedValue: '' })
        break
      default:
        return
    }
  }

  render() {
    const changeSearchInKey = () => {
      const searchInKey = this.state.searchIn === 'title' ? 'body' : 'title'
      this.setState({ searchIn: searchInKey })
    }

    const { items } = this.props
    return (
      <Downshift
        onChange={this.onChange}
        itemToString={() =>
          items && items.map(item => item[this.state.searchIn])
        }
        render={({
          getRootProps,
          getInputProps,
          getItemProps,
          isOpen,
          inputValue,
          selectedItem,
          highlightedIndex
        }) => {
          // removes content, if the input value doesn't match
          const filtred =
            items &&
            items.filter(
              i =>
                !inputValue ||
                (typeof inputValue === 'string' &&
                  i[this.state.searchIn]
                    .toLowerCase()
                    .includes(inputValue.toLowerCase()))
            )
          return (
            <div>
              <SearchWrapper>
                <SearchInFilter>
                  <button onClick={changeSearchInKey}>
                    <Handle xPos={this.state.searchIn === 'title'} />
                  </button>
                </SearchInFilter>
                <InputStyled
                  {...getInputProps({
                    placeholder: `Search in ${this.state.searchIn}`,
                    onKeyDown: this.handleKeyDown
                  })}
                />
                {isOpen ? (
                  <Results>
                    {filtred &&
                      filtred.map((item, index) => (
                        <div
                          {...getItemProps({ item })}
                          key={`searchItem${index}`}
                        >
                          <a href={`${item.url}`} target="_blank">
                            <StateLabel>{item.state}</StateLabel>
                            {item[this.state.searchIn]}
                          </a>
                        </div>
                      ))}
                    {/*outputs `No results` if the filtered array is undefined or empty*/}
                    {filtred ? (
                      filtred.length === 0 && <NoResults>No results</NoResults>
                    ) : (
                      <NoResults>No results</NoResults>
                    )}
                  </Results>
                ) : null}
              </SearchWrapper>
            </div>
          )
        }}
      />
    )
  }
}

export default AutoSuggest
