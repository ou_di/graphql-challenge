/**
 * AutoSuggest test
 * @author Oleg Durybaba <mail@oleg.durybaba.com>
 */

// setup file
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import React from 'react'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import AutoSuggest from './AutoSuggest'

configure({ adapter: new Adapter() })

const createWrapper = (addStore = {}, addProps = {}) => {
  const state = {
      searchIn: 'title'
    },
    options = { context: { state } }

  return {
    wrapper: shallow(<AutoSuggest {...addProps} />, options)
  }
}

// snapshot test
describe('<AutoSuggest />', () => {
  it('renders the issues autosuggest', () => {
    const tree = renderer.create(<AutoSuggest />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('mounting <AutoSuggest />', () => {
  it('renders AutoSuggest without crashing', () => {
    const { wrapper } = createWrapper()
    expect(wrapper.length).toBe(1)
  })
})
