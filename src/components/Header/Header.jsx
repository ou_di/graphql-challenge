// @flow

/**
 * Header component
 * @author Oleg Durybaba <mail@oleg.durybaba.com>
 */

import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import Navigation from '../Navigation'

const HeaderStyled = styled.header`
  padding: 1rem;
  background: ${p => p.theme.color4};
  border-bottom: 1px solid ${p => p.theme.color5};
`

const H1 = styled.h1`
  text-align: center;
  padding: 0.5rem 0;
  margin: 0;
`

const Header = () => {
  return (
    <HeaderStyled>
      <H1>
        <Link to="/">GraphQL Challenge</Link>
      </H1>
      <Navigation />
    </HeaderStyled>
  )
}

export default Header
