// @flow

/**
 * IssuesSearch component
 * The component to filter and output the search results
 * @author Oleg Durybaba <mail@oleg.durybaba.com>
 */

import React, { PureComponent, Fragment } from 'react'
import AutoSuggest from '../AutoSuggest'
import styled from 'styled-components'

const FilterBox = styled.div`
  span {
    padding: 0.5rem;
    display: inline-block;
  }
`

type State = {
  issues: {},
  openedIssesChecked: boolean,
  closedIssesChecked: boolean
}

type Props = {
  issues: {
    +edges: []
  }
}

class IssuesSearch extends PureComponent<Props, State> {
  props: Props

  state = {
    issues: this.props.issues.edges.map(issue => issue.node),
    openedIssesChecked: false,
    closedIssesChecked: false
  }

  /**
   * returns filterd or all issues
   * @return {array} - array of issue data
   */
  getFilteredIssues = () => {
    switch (true) {
      // if both (opened and closed) checkboxes are checked
      case this.state.openedIssesChecked && this.state.closedIssesChecked:
        return this.state.issues
      // if opened is checked
      case this.state.openedIssesChecked:
        return this.state.issues.filter(issue => issue.state === 'OPEN')
      // if closed is checked
      case this.state.closedIssesChecked:
        return this.state.issues.filter(issue => issue.state === 'CLOSED')
      // if no filter is set
      default:
        return this.state.issues
    }
  }

  render() {
    return (
      <Fragment>
        <h2>Facebook/React</h2>
        <FilterBox>
          Filter:{' '}
          <span>
            <input
              type="checkbox"
              onChange={e =>
                this.setState({ openedIssesChecked: e.target.checked })
              }
            />only opened issues
          </span>
          <span>
            <input
              type="checkbox"
              onChange={e =>
                this.setState({ closedIssesChecked: e.target.checked })
              }
            />only closed issues
          </span>
        </FilterBox>
        <AutoSuggest items={this.getFilteredIssues()} />
      </Fragment>
    )
  }
}

export default IssuesSearch
