/**
 * About component
 * @author Oleg Durybaba <mail@oleg.durybaba.com>
 */

import React from 'react'

const About = () => {
  return (
    <div>
      <h2>Challenge for the job opening Senior Frontend Engineer</h2>
      <p>
        This scenario is meant to test your every capability in programming
        modern day applications. Specifically, we want you to build a frontend
        for a GraphQL API. The result of your little scenario is then presented
        by you in person.
      </p>
      <p>
        We want you to build a frontend for the GitHub GraphQL API. The frontend
        should allow to browse the issues of a GitHub repository, specifically
        of the official React repository. You will build a little search which
        allows to search for a text term in either the body or title of the
        issues as well as for the status OPEN or CLOSED. After listing these
        issues, a user should be able to view a single issue and all the
        comments. Please ensure that sufficient queries of users are faster than
        initial requests with a reasonable caching strategy.
      </p>
      <p>
        Every important aspect of the application shall be tested. Rules to
        programming style apply as usual and documentation shall be down to a
        minimum. Your application should be able to handle wrong input by the
        user or any unusual behavior.
      </p>
      <p>
        For this scenario we expect you to deliver a high quality web
        application. Your code should be typed using either Typescript or Flow.
        Furthermore, we expect you to use all available ES6+ features, including
        Async programming style. Your implementation is based on React with a
        structured state management using Redux and a sensible routing strategy.
        Alternatively, you may apply Apollo and appropriate client side state
        management.
      </p>
    </div>
  )
}

export default About
