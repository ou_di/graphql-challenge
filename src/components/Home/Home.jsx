// @flow

/**
 * Home component
 * @author Oleg Durybaba <mail@oleg.durybaba.com>
 */

import React from 'react'
import gql from 'graphql-tag'
import { Query, withApollo } from 'react-apollo'
import IssuesSearch from '../IssuesSearch'

const query = gql`
  query readIssues {
    repositoryOwner(login: "facebook") {
      repository(name: "react") {
        issues(last: 100) {
          totalCount
          pageInfo {
            hasNextPage
            endCursor
          }
          edges {
            node {
              id
              title
              body
              state
              url
            }
          }
        }
      }
    }
  }
`

const Home = props => {
  return (
    <Query query={query}>
      {({ loading, error, data }) => {
        if (loading) return 'Loading...'
        if (error) {
          const cachedData = props.client.readQuery({ query })
          return cachedData ? (
            <IssuesSearch
              issues={cachedData.repositoryOwner.repository.issues}
            />
          ) : (
            `Error! ${error.message}`
          )
        }
        return <IssuesSearch issues={data.repositoryOwner.repository.issues} />
      }}
    </Query>
  )
}

export default withApollo(Home)
