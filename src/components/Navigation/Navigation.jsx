/**
 * Navigation component
 * Uses react router's NavLink component
 * @author Oleg Durybaba <mail@oleg.durybaba.com>
 */

import React from 'react'
import { NavLink } from 'react-router-dom'
import styled from 'styled-components'

const MainNav = styled.nav`
  ul {
    display: flex;
    justify-content: center;

    li {
      padding: 0 0.5rem;
      a {
        color: ${p => p.theme.color0};
      }
    }
  }
`

const Navigation = () => {
  return (
    <MainNav>
      <ul>
        <li>
          <NavLink to="/home" activeStyle={{ color: '#256fd5' }}>
            Home
          </NavLink>
        </li>
        <li>
          <NavLink to="/about" activeStyle={{ color: '#256fd5' }}>
            About the challenge
          </NavLink>
        </li>
      </ul>
    </MainNav>
  )
}

export default Navigation
