This project was bootstrapped with
[Create React App](https://github.com/facebookincubator/create-react-app).

# GraphQL challenge

The app is an example of a graphql usage along with react framework and apollo
library. The App should allow to browse the issues of a GitHub repository of the
official React repository. There should be a search bar which allows to search
for a text term in either the body or title of the issues as well as for the
status OPEN or CLOSED.

## Basic setup

* Babel compiler
* Webpack

## App setup

* [The app is based on React (ejected)](https://reactjs.org/)
* [Apollo is the state/data handling management system](https://www.apollographql.com/docs/react/)
* [Query language API GraphQL](https://graphql.org/)

## Third party libraries

* [React router 4](https://reacttraining.com/react-router/)
* [Styled components](https://www.styled-components.com/)
* [Downshift autosuggest library](https://github.com/paypal/downshift)

## Testing:

* [Jest](https://facebook.github.io/jest/) and
  [Enzyme](https://github.com/airbnb/enzyme)

##Type checker

* [Flow](https://flow.org/en/)

### Todos

* Text/words highlighting while searching
* More functional tests
